<?php
/*
Plugin Name: Reporteador de Alumnos
Plugin URL: http://digit.mx
Description: Plugin para generar los reportes de los Alumnos
Version: 1.0
Author: @milioh
Author URI: http://twitter.com/milioh
Contributors:
Text Domain:
*/

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
// plugin folder url
if(!defined('RC_SCD_PLUGIN_URL')) {
define('RC_SCD_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}

/*
|--------------------------------------------------------------------------
| MAIN CLASS
|--------------------------------------------------------------------------
*/

class rc_milioh_dashboard {

	/*--------------------------------------------*
	 * Constructor
	 *--------------------------------------------*/

	/**
	 * Initializes the plugin
	 */
	function __construct() {

		add_action('admin_menu', array( &$this,'rc_scd_register_menu') );

	} // end constructor

	function rc_scd_register_menu() {
		add_dashboard_page( 'Reporteador de Alumnos', 'Reporteador de Alumnos', 'read', 'reportes-alumnos', array( &$this,'rc_scd_create_dashboard_alumnos') );
	}

	function rc_scd_create_dashboard_alumnos() { include_once( 'alumnos/reportes.php'  ); }
}

// instantiate plugin's class
$GLOBALS['milioh_dashboard'] = new rc_milioh_dashboard();

?>
