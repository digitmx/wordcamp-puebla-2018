<?php
	//CARGAMOS LAS FUNCIONES DE WORDPRESS
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once( $parse_uri[0] . 'wp-load.php' );
	
	//VERIFICAMOS QUE SE HAYAN ENVIADO DATOS
	if (count($_POST) > 0)
	{
		//LEEMOS LOS DATOS ENVIADOS
		$postFechaInicio = (isset($_POST['fechaInicio'])) ? (string)trim($_POST['fechaInicio']) : '';
		$postFechaTermino = (isset($_POST['fechaTermino'])) ? (string)trim($_POST['fechaTermino']) : '';
	
		//VERIFICAMOS QUE LOS DATOS SEAN CORRECTOS
		if ($postFechaInicio && $postFechaTermino)
		{
			//AGREAMOS HORAS A LAS FECHAS
			//$postFechaInicio.= ' 00:00:00';
			//$postFechaTermino.= ' 23:59:59';
			$start_array = explode('-', $postFechaInicio);
			$end_array = explode('-', $postFechaTermino);
			
			//LEEMOS LOS DATOS DE CORREO
			$args = array(
				'post_type' 		=> 'alumno',
				'posts_per_page' 	=> -1,
				'date_query' => array(
					array(
						'after'     => array(
							'year' => (int)$start_array[0],
							'month' => (int)$start_array[1],
							'day' => (int)$start_array[2],
							'hour' => 0,
							'minute' => 0,
							'second' => 0,
						),
						'before'    => array(
							'year' => (int)$end_array[0],
							'month' => (int)$end_array[1],
							'day' => (int)$end_array[2],
							'hour' => 23,
							'minute' => 59,
							'second' => 59,
						),
						'inclusive' => true,
					),
				)
			);
			$alumnos = get_posts($args);
			
			//VERIFICAMOS SI HAY RESULTADOS
			if (count($alumnos) > 0)
			{
				echo '<table class="wp-list-table widefat pages">';
				echo '	<thead>';
				echo '		<tr>';
				echo '			<th scope="col" id="credencial" style="">';
				echo '				<span>Credencial</span>';
				echo '			</th>';
				echo '			<th scope="col" id="nombre" style="">';
				echo '				<span>Nombre</span>';
				echo '			</th>';
				echo '			<th scope="col" id="apellidos" style="">';
				echo '				<span>Apellidos</span>';
				echo '			</th>';
				echo '			<th scope="col" id="edad" style="">';
				echo '				<span>Edad</span>';
				echo '			</th>';
				echo '			<th scope="col" id="sexo" style="">';
				echo '				<span>Sexo</span>';
				echo '			</th>';
				echo '		</tr>';
				echo '	</thead>';
				echo '	<tbody>';
				foreach ($alumnos as $alumno) 
				{
					echo '		<tr>';
					echo '			<td>'.get_post_meta($alumno->ID, "credencial", true).'</td>';
					echo '			<td>'.get_post_meta($alumno->ID, "nombre", true).'</td>';
					echo '			<td>'.get_post_meta($alumno->ID, "apellidos", true).'</td>';
					echo '			<td>'.get_post_meta($alumno->ID, "edad", true).'</td>';
					echo '			<td>'.get_post_meta($alumno->ID, "sexo", true).'</td>';
					echo '		</tr>';
				}
				echo '	</tbody>';	
				echo '	<tfoot>';
				echo '		<tr>';
				echo '			<td colspan="5" align="right">'.count($alumnos).' elementos</td>';
				echo '		</tr>';
				echo '	</tfoot>';
				echo '</table>';
			}
			else
			{
				//MENSAJE CUANDO NO HAY RESULTADOS
				echo 'No se encontraron resultados en esta búsqueda. Cambia las Fechas o Resetea la Búsqueda.';
			}
		}
		else
		{
			//MENSAJE DE ERROR
			echo 'error';
		}
	}
	else
	{
		//MENSAJE DE ERROR
		echo 'error';
	}
	
?>