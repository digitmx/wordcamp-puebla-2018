<?php
/**
 * Our custom dashboard page
 */

/** WordPress Administration Bootstrap */
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );

$url = RC_SCD_PLUGIN_URL;
//$url = str_replace('http', 'https', $url); /* Habilitar en caso de que tu sitio tenga certificado SSL */

wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
?>

<div class="wrap about-wrap">
	<h1><?php _e( 'Reporteador de Alumnos' ); ?></h1>

	<div class="about-text">
		<?php _e('Aquí podrás obtener la lista de los alumnos para descargar en CSV.' ); ?>
	</div>

	<?php
		//LEEMOS LOS DATOS DE ALUMNOS
		$args = array(
			'post_type' 		=> 'alumno',
			'posts_per_page' 	=> 10,
			'orderby'			=> 'ID',
			'order'				=> 'DESC'
		);
		$alumnos = get_posts($args);
	?>

	<div class="changelog">
		<div class="feature-section images-stagger-right">
			<form id="formParticipantes" accept-charset="utf-8" method="post" style="margin-bottom: 20px !important; ">
				<legend>Filtro de Consulta de Alumnos</legend>
				<input type="date" class="custom_date" id="fechaInicio" name="fechaInicio" placeholder="Fecha de Inicio" />
				<input type="date" class="custom_date" id="fechaTermino" name="fechaTermino" placeholder="Fecha de Termino" />
				<button type="submit" id="btnConsultarAlumnos" name="btnConsultarAlumnos">Consultar Alumnos</button>
				<button type="button" id="btnReiniciarAlumnos" name="btnReiniciarAlumnos">Resetear Alumnos</button>
				<button type="button" id="btnGenerarExcel" name="btnGenerarExcel" style="background-color:#287B4D; color: #FFF;">Generar Excel</button>
				<button type="button" id="btnBajarExcel" name="btnBajarExcel" style="background-color:#E52B43; color: #FFF;">Descargar CSV</button>
			</form>
			<div class="alumnosTabla">
				<table class="wp-list-table widefat pages">
					<thead>
						<tr>
							<th scope="col" id="credencial" style="">
								<span>Credencial</span>
							</th>
							<th scope="col" id="nombre" style="">
								<span>Nombre</span>
							</th>
							<th scope="col" id="apellidos" style="">
								<span>Apellidos</span>
							</th>
							<th scope="col" id="edad" style="">
								<span>Edad</span>
							</th>
							<th scope="col" id="sexo" style="">
								<span>Sexo</span>
							</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($alumnos as $alumno) { ?>
						<tr>
							<td><?php echo get_post_meta($alumno->ID, "credencial", true); ?></td>
							<td><?php echo get_post_meta($alumno->ID, "nombre", true); ?></td>
							<td><?php echo get_post_meta($alumno->ID, "apellidos", true); ?></td>
							<td><?php echo get_post_meta($alumno->ID, "edad", true); ?></td>
							<td><?php echo get_post_meta($alumno->ID, "sexo", true); ?></td>
						</tr>
						<?php } ?>
					</tbody>

					<tfoot>
						<tr>
							<td colspan="5" align="right">Últimos <?php echo count($alumnos); ?> elementos</td>
						</tr>
					</tfoot>

				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		//Funcion para mostrar el DatePicker
		$('.custom_date').datepicker({
			dateFormat : 'yy-mm-dd'
		});

		//Función para el boton de consulta de Alumnos
		$("#btnConsultarAlumnos").on('click', function(e) {
			e.preventDefault();

			//Leemos las Variables
			var inicio = $("#fechaInicio").val();
			var termino = $("#fechaTermino").val();

			//Verificamos
			if (inicio != '' && termino != '')
			{
				//Verificamos que sea mayor termino
				if (termino >= inicio)
				{
					//Procesamos el Formulario
					$.post( "<?php echo $url; ?>alumnos/query-alumnos.php", { fechaInicio : inicio, fechaTermino : termino }, function( data ) {
						if (data != 'error')
						{
							//Cambiamos el HTML de la Tabla
							$(".alumnosTabla").html(data);
						}
						else
						{
							//Mensaje de error
							alert('No se pudieron leer los parámetros. Intenta más tarde.');
						}
					});
				}
				else
				{
					//Alerta de Error
					alert('La Fecha de Término debe ser igual o mayor que la Fecha de Inicio');
				}
			}
			else
			{
				//Alerta de Error
				alert('Necesitas llenar los campos de fecha de inicio y fecha de término');
			}

			return false;
		});

		//Función para resetear la consulta de Alumnos
		$("#btnReiniciarAlumnos").on('click', function(e) {
			e.preventDefault();

			location.reload();

			return false;
		});

		//Función para generar el archivo CSV
		$("#btnGenerarExcel").on('click', function(e) {
			e.preventDefault();

			//Leemos las Variables
			var inicio = $("#fechaInicio").val();
			var termino = $("#fechaTermino").val();

			//Procesamos el Formulario
			$.post( "<?php echo $url; ?>alumnos/create-csv.php", { fechaInicio : inicio, fechaTermino : termino }, function( data ) {
				if (data == 'error')
				{
					//Mensaje de error
					alert('No se pudieron leer los parámetros. Intenta más tarde.');
				}
				else
				{
					console.log(data);
					alert('Archivo CSV generado con éxito.');
				}
			});

			return false;
		});

		//Función para descargar el archivo CSV
		$("#btnBajarExcel").on('click', function(e) {
			//Procesamos el Formulario
			window.open("<?php echo $url; ?>alumnos/download.php?file=alumnos.csv",'_blank');
		});

	});
</script>