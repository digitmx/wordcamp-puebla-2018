<?php
	//CARGAMOS LAS FUNCIONES DE WORDPRESS
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once( $parse_uri[0] . 'wp-load.php' );

	//CREAMOS EL ARREGLO DE ALUMNOS
	$resultado_alumnos = array();

	//VERIFICAMOS QUE SE HAYAN ENVIADO DATOS
	if (count($_POST) > 0)
	{
		//LEEMOS LOS DATOS ENVIADOS
		$postFechaInicio = (isset($_POST['fechaInicio'])) ? (string)trim($_POST['fechaInicio']) : '';
		$postFechaTermino = (isset($_POST['fechaTermino'])) ? (string)trim($_POST['fechaTermino']) : '';

		//VERIFICAMOS QUE LOS DATOS SEAN CORRECTOS
		if ($postFechaInicio && $postFechaTermino)
		{

			//AGREAMOS HORAS A LAS FECHAS
			//$postFechaInicio.= ' 00:00:00';
			//$postFechaTermino.= ' 23:59:59';
			$start_array = explode('-', $postFechaInicio);
			$end_array = explode('-', $postFechaTermino);

			//LEEMOS LOS DATOS DE ALUMNOS
			$args = array(
				'post_type' 		=> 'alumno',
				'posts_per_page' 	=> -1,
				'date_query' => array(
					array(
						'after'     => array(
							'year' => (int)$start_array[0],
							'month' => (int)$start_array[1],
							'day' => (int)$start_array[2],
							'hour' => 0,
							'minute' => 0,
							'second' => 0,
						),
						'before'    => array(
							'year' => (int)$end_array[0],
							'month' => (int)$end_array[1],
							'day' => (int)$end_array[2],
							'hour' => 23,
							'minute' => 59,
							'second' => 59,
						),
						'inclusive' => true,
					),
				)
			);
			$alumnos = get_posts($args);

			//VERIFICAMOS QUE EXISTAN LOS ALUMNOS
			if (count($alumnos) > 0)
			{
				//PROCESAMOS LOS ALUMNOS
				foreach ($alumnos as $post)
				{
					$credencial = get_post_meta($post->ID, "credencial", true);
					$nombre = get_post_meta($post->ID, "nombre", true);
					$apellidos = get_post_meta($post->ID, "apellidos", true);
					$edad = get_post_meta($post->ID, "edad", true);
					$sexo = get_post_meta($post->ID, "sexo", true);

					//GENERAMOS LOS ELEMENTOS
					$resultado_alumnos[] = array($credencial, $nombre, $apellidos, $edad, $sexo);
				}

				// Set header row values
				$csv_fields=array();
				$csv_fields[] = 'Credencial';
				$csv_fields[] = 'Nombre';
				$csv_fields[] = 'Apellidos';
				$csv_fields[] = 'Edad';
				$csv_fields[] = 'Sexo';
				$output_filename = 'alumnos.csv';
				echo $output_handle = @fopen( 'alumnos.csv', 'w' );

				header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
				header( 'Content-Description: File Transfer' );
				header( 'Content-type: text/csv' );
				header( 'Content-Disposition: attachment; filename=' . $output_filename );
				header( 'Expires: 0' );
				header( 'Pragma: public' );

				// Insert header row
				fputcsv( $output_handle, $csv_fields );

				// Parse results to csv format
				foreach ($resultado_alumnos as $Result)
				{
					$leadArray = (array) $Result; // Cast the Object to an array
					// Add row to file
					$leadArray = array_map("utf8_decode", $leadArray);
					fputcsv( $output_handle, $leadArray );
				}

				// Close output file stream
				fclose( $output_handle );

				die();
			}
			else
			{
				//MENSAJE DE ERROR
				echo 'error';
			}
		}
		else
		{
			//LEEMOS LOS ALUMNOS
			$args = array(
				'post_type' 		=> 'alumno',
				'posts_per_page' 	=> 10,
				'orderby'			=> 'ID',
				'order'				=> 'DESC'
			);
			$alumnos = get_posts($args);

			//VERIFICAMOS QUE EXISTAN LOS ALUMNOS
			if (count($alumnos) > 0)
			{
				//PROCESAMOS LOS ALUMNOS
				foreach ($alumnos as $post)
				{
					$credencial = get_post_meta($post->ID, "credencial", true);
					$nombre = get_post_meta($post->ID, "nombre", true);
					$apellidos = get_post_meta($post->ID, "apellidos", true);
					$edad = get_post_meta($post->ID, "edad", true);
					$sexo = get_post_meta($post->ID, "sexo", true);

					//GENERAMOS LOS ELEMENTOS
					$resultado_alumnos[] = array($credencial, $nombre, $apellidos, $edad, $sexo);
				}

				// Set header row values
				$csv_fields=array();
				$csv_fields[] = 'Credencial';
				$csv_fields[] = 'Nombre';
				$csv_fields[] = 'Apellidos';
				$csv_fields[] = 'Edad';
				$csv_fields[] = 'Sexo';
				$output_filename = 'alumnos.csv';
				echo $output_handle = @fopen( 'alumnos.csv', 'w' );

				header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
				header( 'Content-Description: File Transfer' );
				header( 'Content-type: text/csv' );
				header( 'Content-Disposition: attachment; filename=' . $output_filename );
				header( 'Expires: 0' );
				header( 'Pragma: public' );

				// Insert header row
				fputcsv( $output_handle, $csv_fields );

				// Parse results to csv format
				foreach ($resultado_alumnos as $Result)
				{
					$leadArray = (array) $Result; // Cast the Object to an array
					// Add row to file
					$leadArray = array_map("utf8_decode", $leadArray);
					fputcsv( $output_handle, $leadArray );
				}

				// Close output file stream
				fclose( $output_handle );

				die();
			}
			else
			{
				//MENSAJE DE ERROR
				echo 'error';
			}
		}
	}
	else
	{
		//LEEMOS LOS DATOS DE ALUMNOS
		$args = array(
			'post_type' 		=> 'alumno',
			'posts_per_page' 	=> 10,
			'orderby'			=> 'ID',
			'order'				=> 'DESC'
		);
		$alumnos = get_posts($args);

		//VERIFICAMOS QUE EXISTAN LOS ALUMNOS
		if (count($alumnos) > 0)
		{
			//PROCESAMOS LOS ALUMNOS
			foreach ($alumnos as $post)
			{
				$credencial = get_post_meta($post->ID, "credencial", true);
				$nombre = get_post_meta($post->ID, "nombre", true);
				$apellidos = get_post_meta($post->ID, "apellidos", true);
				$edad = get_post_meta($post->ID, "edad", true);
				$sexo = get_post_meta($post->ID, "sexo", true);

				//GENERAMOS LOS ELEMENTOS
				$resultado_alumnos[] = array($credencial, $nombre, $apellidos, $edad, $sexo);
			}

			// Set header row values
			$csv_fields=array();
			$csv_fields[] = 'Credencial';
			$csv_fields[] = 'Nombre';
			$csv_fields[] = 'Apellidos';
			$csv_fields[] = 'Edad';
			$csv_fields[] = 'Sexo';
			$output_filename = 'alumnos.csv';
			echo $output_handle = @fopen( 'alumnos.csv', 'w' );

			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-type: text/csv' );
			header( 'Content-Disposition: attachment; filename=' . $output_filename );
			header( 'Expires: 0' );
			header( 'Pragma: public' );

			// Insert header row
			fputcsv( $output_handle, $csv_fields );

			// Parse results to csv format
			foreach ($resultado_alumnos as $Result)
			{
				$leadArray = (array) $Result; // Cast the Object to an array
				// Add row to file
				$leadArray = array_map("utf8_decode", $leadArray);
				fputcsv( $output_handle, $leadArray );
			}

			// Close output file stream
			fclose( $output_handle );

			die();
		}
		else
		{
			//MENSAJE DE ERROR
			echo 'error';
		}
	}
?>